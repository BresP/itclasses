
  exec { 'prepare_apt':
    command => '/usr/bin/apt-get update'
  }

  package {
    'vim':
      ensure    => 'present',
      require   => Exec['prepare_apt'];
    'git':
      ensure    => 'present',
      require   => Exec['prepare_apt'];
    'mc':
      ensure    => 'present',
      require   => Exec['prepare_apt'];
    'htop':
      ensure    => 'present',
      require   => Exec['prepare_apt'];
    'iftop':
      ensure    => 'present',
      require   => Exec['prepare_apt'];
    'chromium-browser':
      ensure    => 'present',
      require   => Exec['prepare_apt'];
    'xfce4':
      ensure    => 'present',
      require   => Exec['prepare_apt'];
    'gdm':
      ensure    => 'present',
      subscribe => Package['xfce4'],
      require   => Exec['prepare_apt']
  }

  exec {'configure_graphical_boot':
    command     => '/usr/sbin/dpkg-reconfigure gdm',
    subscribe   => Package['gdm'],
    refreshonly => true
  }

  file {
    '/vagrant/flask-login-example/flask-login-example.log':
      ensure  => 'present',
      require => Exec['flask_login_app_checkout'],
      before  => Exec['update_host_for_flask_example'];
  }

  exec {'pip_download':
    command => '/usr/bin/wget https://bootstrap.pypa.io/get-pip.py -q',
    cwd     => '/vagrant',
    creates => '/vagrant/get-pip.py',
    user    => 'vagrant';
  }
  exec {'install_pip':
    command     => '/usr/bin/python3.4 /vagrant/get-pip.py',
    subscribe   => Exec['pip_download'];
  }
  exec {'install_flask':
    command     => '/usr/local/bin/pip3.4 install Flask Flask-Login',
    subscribe   => Exec['install_pip'];
  }
  exec {'flask_login_app_checkout':
    command => '/usr/bin/git clone https://github.com/shekhargulati/flask-login-example.git',
    creates => '/vagrant/flask-login-example',
    user    => 'vagrant',
    cwd     => '/vagrant';
  }
  exec {'update_host_for_flask_example':
    command     => '/bin/sed -in "s/run()/run(host=\'0.0.0.0\')/g" /vagrant/flask-login-example/flask-login-example.py | exit 0',
    subscribe   => Exec['flask_login_app_checkout'],
    notify      => Service['supervisor'],
    refreshonly => true;
  }
  
  include 'supervisor'
