class supervisor {
  package {
    'supervisor':
      ensure    => 'present';
  }

  file {
    '/etc/supervisor/conf.d':
      ensure  => 'directory',
      require => Package['supervisor'];
    '/etc/supervisor/conf.d/example1.conf':
      ensure  => 'present',
      source  => 'puppet:///modules/supervisor/example1.conf.source',
      require => File['/etc/supervisor/conf.d'],
      notify  => Service['supervisor'];
  }

  service {
    'supervisor':
      ensure  => 'running',
      enable  => true,
      require => Package['supervisor'];
  }
}
