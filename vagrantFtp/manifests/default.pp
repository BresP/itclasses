
  exec { 'prepare_apt':
    command => '/usr/bin/apt-get update'
  }

  package {
    'vim':
      ensure => 'present';
    'chromium-browser':
      ensure => 'present',
      require => Exec['prepare_apt'];
    'vsftpd':
      ensure => 'present',
      require => Exec['prepare_apt'];
    'xfce4':
      ensure => 'present',
      require => Exec['prepare_apt'];
    'gdm':
      ensure => 'present',
      subscribe => Package['xfce4'],
      require => Exec['prepare_apt']
  }

  exec {'configure_graphical_boot':
    command => '/usr/sbin/dpkg-reconfigure gdm',
    subscribe => Package['gdm'],
    refreshonly => true
  }

